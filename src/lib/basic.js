var BaseUtil={
/**
 * 是否为函数
 * @param  {*} fn 对象
 * @return {Boolean}  是否函数
 */
isFunction : function(fn){
    return typeof(fn) === 'function';
},
/**
 * 是否数组
 * @method
 * @param  {*}  obj 是否数组
 * @return {Boolean}  是否数组
 */
isArray : ('isArray' in Array) ? Array.isArray : function(value) {
    return toString.call(value) === '[object Array]';
},
/**
 * 是否日期
 * @param  {*}  value 对象
 * @return {Boolean}  是否日期
 */
isDate: function(value) {
    return toString.call(value) === '[object Date]';
},
/**
 * 是否是javascript对象
 * @param {Object} value The value to test
 * @return {Boolean}
 * @method
 */
isObject: (toString.call(null) === '[object Object]') ?
    function(value) {
        // check ownerDocument here as well to exclude DOM nodes
        return value !== null && value !== undefined && toString.call(value) === '[object Object]' && value.ownerDocument === undefined;
    } :
    function(value) {
        return toString.call(value) === '[object Object]';
    },
 /**
 * 是否是数字或者数字字符串
 * @param  {String}  value 数字字符串
 * @return {Boolean}  是否是数字或者数字字符串
 */
isNumeric: function(value) {
    return !isNaN(parseFloat(value)) && isFinite(value);
},
/**
 * 抛出错误
 */
error : function(msg){
    if(BaseUtil.debug){
        throw msg;
    }
},
 /**
 * 生成唯一的Id
 * @method
 * @param {String} prefix 前缀
 * @default 'BaseUtil-guid'
 * @return {String} 唯一的编号
 */
guid : (function(){
    var map = {};
    return function(prefix){
        prefix = prefix || BaseUtil.prefix + GUID_DEFAULT;
        if(!map[prefix]){
            map[prefix] = 1;
        }else{
            map[prefix] += 1;
        }
        return prefix + map[prefix];
    };
})(),
/**
 * 判断是否是字符串
 * @return {Boolean} 是否是字符串
 */
isString : function(value){
    return typeof value === 'string';
},

/**
 * 判断是否数字，由于$.isNumberic方法会把 '123'认为数字
 * @return {Boolean} 是否数字
 */
isNumber : function(value){
    return typeof value === 'number';
},
/**
 * 是否是布尔类型
 *
 * @param {Object} 测试的值
 * @return {Boolean}
 */
isBoolean: function(value) {
    return typeof value === 'boolean';
},
 /**
 * 使第一个字母变成大写
 * @param  {String} s 字符串
 * @return {String} 首字母大写后的字符串
 */
ucfirst : function(s){
    s += '';
    return s.charAt(0).toUpperCase() + s.substring(1);
},
/**
  * [isEvenNum description]
  * @authors zhanwang.ye
  * @param  {[type]}  a [description]
  * @return {Boolean}   [description]
  */
 isEvenNum:function(a){
    if(Number(a)==NaN) return false;
    return a%2>0?false:true;
 },
 /**
  * [isOddNum description]
  * @authors zhanwang.ye
  * @param  {[type]}  a [description]
  * @return {Boolean}   [description]
  */
 isOddNum:function(a){
    if(Number(a)==NaN) return false;
    return a%2>0?true:false;
 }

};

module.exports=BaseUtil;
