## vui(beta) 



### 移动端UI框架，基于Vue.js实现。

持续更新移动webapp项目所需的常用组件

目前是beta版本,有些规范还在优化中,升级时请注意留意文档更新说明

### 使用文档

http://area.oschina.io/vue-zui/#!/

### vui 组件库

![输入图片说明](http://git.oschina.net/uploads/images/2016/0727/003549_0961b45b_331468.png "在这里输入图片标题")




## Browser
	
        <link href="../dist/vui.min.css" rel="stylesheet">
        <script src="../dist/vue.js"></script>
        <script src="../dist/vui.min.js"></script>
        <script>
        var Toast= vui.Toast
        var app = new Vue({
            el : '#app',
            components:{
 		Toast
 	    }
        });
        </script>

## CommonJS
        
        

	<city-select :visible="isCityVisible" @change="changeCity" @close="closeCity"></city-select>

	import citySelect from "vue-zui/src/components/select/citySelect/"
	
	new Vue({
		el:"#app",
		components:{
			citySelect:citySelect
		}
	});



## nodeJs

npm install vue-zui

